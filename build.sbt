name := "scala-decision-tree"; 

version := "0.0.0"; 

scalaVersion := "2.10.6";

libraryDependencies ++= Seq(
  "io.prediction"    %% "core"          % "0.9.4" % "provided",
  "org.apache.spark" %% "spark-core"    % "1.6.3" % "provided",
  "org.apache.spark" %% "spark-mllib"   % "1.6.3" % "provided",
  "io.spray" %% "spray-json" % "1.2.6"
  )
  
