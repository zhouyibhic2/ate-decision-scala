package com.bigtester.ate.tcg.ml

import io.prediction.controller.PDataSource
import io.prediction.controller.EmptyEvaluationInfo
import io.prediction.controller.EmptyActualResult
import io.prediction.controller.Params
import io.prediction.data.storage.Event
import io.prediction.data.storage.Storage

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors

import grizzled.slf4j.Logger

class Query(
  val features: Array[Double]
) extends Serializable

class PredictedResult(
  val value: Double
) extends Serializable

class ActualResult(
  val value: Double
) extends Serializable

case class Record(target: Double, attr0: Double,attr1: String,attr2: Double,attr3: Double,attr4: Double,attr5: Double,attr6: Double,attr7: Double,attr8: String,attr9: Double,attr10: Double,attr11: Double,attr12: Double)

case class DataSourceParams(
  appId: Int,
  evalK: Option[Int]  // define the k-fold parameter.
) extends Params

class DataSource(val dsp: DataSourceParams)
  extends PDataSource[TrainingData, EmptyEvaluationInfo, Query, ActualResult] {

  @transient lazy val logger = Logger[this.type]

  def readFromDbAndFile3(sc: SparkContext): org.apache.spark.sql.DataFrame = {
    //Storage.config
    val eventsDb = Storage.getPEvents()
    val eRdd = io.prediction.data.store.PEventStore.find(appName="MyApp1")(sc)
    val labeledPoints: RDD[Record] = eventsDb.aggregateProperties(
      appId = dsp.appId,
      entityType = "row",
      // only keep entities with these required properties defined
      required = Some(List(
        "target", "attr0", "attr1", "attr2", "attr3", 
        "attr4", "attr5", "attr6", "attr7", "attr8",
        "attr9", "attr10", "attr11", "attr12")))(sc)
      // aggregateProperties() returns RDD pair of
      // entity ID and its aggregated properties
      .map { case (entityId, properties) =>
        try {
          Record(properties.get[Double]("target"),
            
              properties.get[Double]("attr0"),
              properties.get[String]("attr1"),
              properties.get[Double]("attr2"),
              properties.get[Double]("attr3"),
              properties.get[Double]("attr4"),
              properties.get[Double]("attr5"),
              properties.get[Double]("attr6"),
              properties.get[Double]("attr7"),
              properties.get[String]("attr8"),
              properties.get[Double]("attr9"),
              properties.get[Double]("attr10"),
              properties.get[Double]("attr11"),
              properties.get[Double]("attr12")
            )
          
        } catch {
          case e: Exception => {
            logger.error(s"Failed to get properties ${properties} of" +
              s" ${entityId}. Exception: ${e}.")
            throw e
          }
        }
      }.cache()
      println(s"Read ${labeledPoints.count} labeled Points from db.")
      val sqlContext= new org.apache.spark.sql.SQLContext(sc)
      import sqlContext.implicits._
      labeledPoints.toDF()

  }
  
  def readFromDbAndFile(sc: SparkContext, filename: String, metadata: String): TrainingData = {
//    val eventsDb = Storage.getPEvents()
//    val labeledPoints: RDD[LabeledPoint] = eventsDb.aggregateProperties(
//      appId = dsp.appId,
//      entityType = "row",
//      // only keep entities with these required properties defined
//      required = Some(List(
//        "target", "attr0", "attr1", "attr2", "attr3", 
//        "attr4", "attr5", "attr6", "attr7", "attr8",
//        "attr9", "attr10", "attr11", "attr12")))(sc)
//      // aggregateProperties() returns RDD pair of
//      // entity ID and its aggregated properties
//      .map { case (entityId, properties) =>
//        try {
//          LabeledPoint(properties.get[Double]("target"),
//            Vectors.dense(Array(
//              properties.get[Double]("attr0"),
//              properties.get[Double]("attr1"),
//              properties.get[Double]("attr2"),
//              properties.get[Double]("attr3"),
//              properties.get[Double]("attr4"),
//              properties.get[Double]("attr5"),
//              properties.get[Double]("attr6"),
//              properties.get[Double]("attr7"),
//              properties.get[Double]("attr8"),
//              properties.get[Double]("attr9"),
//              properties.get[Double]("attr10"),
//              properties.get[Double]("attr11"),
//              properties.get[Double]("attr12")
//            ))
//          )
//        } catch {
//          case e: Exception => {
//            logger.error(s"Failed to get properties ${properties} of" +
//              s" ${entityId}. Exception: ${e}.")
//            throw e
//          }
//        }
//      }.cache()
//      println(s"Read ${labeledPoints.count} labeled Points from db.")

      // read from file
      val textFile = sc.textFile(filename)
      val metadataFile = sc.textFile(metadata)
    
val myFile1 = textFile.map(x=>x.split(",")).map {
  case Array(target, attr0,attr1,attr2,attr3,attr4,attr5,attr6,attr7,attr8,attr9,attr10, attr11, attr12) => Record(target.toDouble, attr0.toDouble,attr1.toString,attr2.toDouble,attr3.toDouble,attr4.toDouble,attr5.toDouble,attr6.toDouble,attr7.toDouble,attr8.toString,attr9.toDouble,attr10.toDouble, attr11.toDouble, attr12.toDouble)
} 

    //val df = myFile1.toDF()
      val labeledPoints2 = textFile
        .map { line =>
          val linesplit = line.split(",")
          val targetStr = linesplit.head
          val features = linesplit.drop(1).map(_.toDouble).toArray

          LabeledPoint(targetStr.toDouble, Vectors.dense(features))
        }.cache()
      println(s"Read ${labeledPoints2.count} labeled Points from file.")

      // read metadata
      // metadata contains the number of categories with "ca" prepend if the
      // feature is categorical and "co" if the the feature is continuous
      // for example: target,ca13,co,ca3,co,co
      val categoricalFeaturesInfo = metadataFile.first.split(",").drop(1).zipWithIndex
      .filter{case (value, index) => value != "co"}
      .map{case (value, index) => index -> value.drop(2).toInt }.toMap

      //val labeledPointsAll = labeledPoints.union(labeledPoints2)
      val labeledPointsAll = labeledPoints2
      val Array(trainingPoints, testingPoints) = labeledPointsAll.randomSplit(Array(0.8, 0.2))
      println(s"Read ${trainingPoints.count} for training.")
      println(s"Read ${testingPoints.count} for testing.")

      new TrainingData(trainingPoints, testingPoints, categoricalFeaturesInfo)

  }
  
    def readFromDbAndFile2(sc: SparkContext, filename: String, metadata: String): org.apache.spark.sql.DataFrame = {
       val textFile = sc.textFile(filename)
      val metadataFile = sc.textFile(metadata)
    

val myFile1 = textFile.map(x=>x.split(",")).map {
  case Array(target, attr0,attr1,attr2,attr3,attr4,attr5,attr6,attr7,attr8,attr9,attr10, attr11, attr12) => Record(target.toDouble, attr0.toDouble,attr1.toString,attr2.toDouble,attr3.toDouble,attr4.toDouble,attr5.toDouble,attr6.toDouble,attr7.toDouble,attr8.toString,attr9.toDouble,attr10.toDouble, attr11.toDouble, attr12.toDouble)
       }
        val sqlContext= new org.apache.spark.sql.SQLContext(sc)
      import sqlContext.implicits._

 
      myFile1.toDF()
  }


  override
  def readTraining(sc: SparkContext): TrainingData = {

    readFromDbAndFile(sc, "/home/peidong/bitbucket/ate-pio-decision-scala/ate-decision-scala/data/learning.csv", "/home/peidong/bitbucket/ate-pio-decision-scala/ate-decision-scala/data/learning_metadata.csv")

  }
  def readTraining2(sc: SparkContext):  org.apache.spark.sql.DataFrame  = {

    readFromDbAndFile2(sc, "/home/peidong/bitbucket/ate-pio-decision-scala/ate-decision-scala/data/learning.csv", "/home/peidong/bitbucket/ate-pio-decision-scala/ate-decision-scala/data/learning_metadata.csv")

  }
  override
  def readEval(sc: SparkContext): Seq[(TrainingData, EmptyEvaluationInfo, RDD[(Query, ActualResult)])] = {
    require(!dsp.evalK.isEmpty, "DataSourceParams.evalK must not be None")

    val trainingData = readFromDbAndFile(sc, "/home/peidong/bitbucket/ate-pio-decision-scala/ate-decision-scala/data/learning.csv", "/home/peidong/bitbucket/ate-pio-decision-scala/ate-decision-scala/data/learning_metadata.csv")

    // K-fold splitting
    val evalK = dsp.evalK.get
    val indexedPoints: RDD[(LabeledPoint, Long)] = trainingData.trainingPoints.zipWithIndex

    (0 until evalK).map { idx => 
      val trainingPoints = indexedPoints.filter(_._2 % evalK != idx).map(_._1)
      val testingPoints = indexedPoints.filter(_._2 % evalK == idx).map(_._1)

      (
        new TrainingData(trainingPoints, trainingPoints, Map[Int, Int]()),
        new EmptyEvaluationInfo(),
        testingPoints.map { 
          p => (new Query(p.features.toArray), new ActualResult(p.label)) 
        }
      )
    }
  }

}

class TrainingData(
  val trainingPoints: RDD[LabeledPoint],
  val testingPoints: RDD[LabeledPoint],
  val categoricalFeaturesInfo: Map[Int, Int]
) extends Serializable