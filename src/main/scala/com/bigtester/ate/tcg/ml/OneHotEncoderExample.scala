// $example on$
package com.bigtester.ate.tcg.ml
import org.apache.spark.ml.feature.{OneHotEncoder, StringIndexer}

import org.apache.spark.ml.Pipeline



object OneHotEncoderExample {
  def main(args: Array[String]): Unit = {
     val configuration = new org.apache.spark.SparkConf()
                .setAppName("Your Application Name")
                .setMaster("local");
    val sc = new org.apache.spark.SparkContext(configuration) // An existing SparkContext.
    
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
   import sqlContext.implicits._

   val fileTrainingData = new DataSource(new DataSourceParams(1, Some(1)))
   val trainingData = fileTrainingData.readFromDbAndFile3(sc);
   //val dfPoints = trainingData.trainingPoints.toDF("target","features")
   //val featuresDf = dfPoints.select("features").rdd
   
//   map {
//  case Array(target, attr0,attr1,attr2,attr3,attr4,attr5,attr6,attr7,attr8,attr9,attr10, attr11, attr12) => Record(target.toDouble, attr0.toDouble,attr1.toDouble,attr2.toDouble,attr3.toDouble,attr4.toDouble,attr5.toDouble,attr6.toDouble,attr7.toDouble,attr8.toDouble,attr9.toDouble,attr10.toDouble, attr11.toDouble, attr12.toDouble)
//} 
//   dfPoints.map(row=>{
//     
//   })
   //featuresDf.toArray()
   
//   val df = trainingData.trainingPoints.toDF("target","co","co","co","ca2","co","co","co","co","ca25","co","co","co","co");
    // $example on$
    val df = trainingData
    //).toDF("id", "category", "testCol")
    //df.show()
    //indexing columns
val stringColumns = Array("attr1","attr8")
val index_transformers: Array[org.apache.spark.ml.PipelineStage] = stringColumns.map(
  cname => new StringIndexer()
    .setInputCol(cname)
    .setOutputCol(s"${cname}_index")
)

// Add the rest of your pipeline like VectorAssembler and algorithm
val index_pipeline = new Pipeline().setStages(index_transformers)
val index_model = index_pipeline.fit(df)
val indexed = index_model.transform(df)

//    
//    val indexer = new StringIndexer()
//      .setInputCol("category")
//      .setOutputCol("categoryIndex")
//      .fit(df)
//    val indexed = indexer.transform(df)
    val indexed1 = indexed.drop("attr1")
    val indexed2 = indexed1.drop("attr8")
    indexed2.show()
    Console.println("abc")
    val encoder = new OneHotEncoder()
      .setInputCol("attr1_index")
      .setOutputCol("attr1_categoryVec")

    val encoded = encoder.transform(indexed)
    
    //encoded.show()
    // $example off$

    //sqlContext.
  }
}